import concat from 'gulp-concat';
import minify from 'gulp-minify';

import { src, dest, parallel } from 'gulp';

const DIST_DIR = './dist/';

function buildJS () {
	return src(['./src/htmlTemplate.js', './src/components/**/*.js'])
		.pipe(concat('nebucomponents.js'))
		.pipe(minify())
		.pipe(dest(DIST_DIR));
};
function buildCSS () {
	return src(['./src/components/**/*.css'])
	.pipe(concat('nebucomponents.css'))
	.pipe(dest(DIST_DIR));
};

const build = parallel(buildJS, buildCSS);

export default build;