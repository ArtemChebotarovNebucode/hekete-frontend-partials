function DatepickerDMY(el, config) {
    config = {
        datesEl: null,
        mthEl: null,
        nextMthEl: null,
        prevMthEl: null,
        daysEl: null,
        calendarButtonEl: null,
        selectedDateInputEl: null,
        ...config
    };

    if (!config.name) throw "Datepicker 'name' parameter is mandatory";

    const template = html`
    <button type="button" class="calendar-button">
        <svg id="Icon_Left_" data-name="Icon (Left)" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16" class="calendar-icon">
            <defs>
                <clipPath id="clip-path">
                    <path id="Mask" d="M14,16H2a2,2,0,0,1-2-2V2A2,2,0,0,1,2,0H4V1A1,1,0,0,0,6,1V0h4V1a1,1,0,0,0,2,0V0h2a2,2,0,0,1,2,2V14A2,2,0,0,1,14,16ZM3,5A1,1,0,0,0,2,6v7a1,1,0,0,0,1,1H13a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1Zm9,7H10V10h2v2ZM9,12H7V10H9v2ZM6,12H4V10H6v2Zm6-3H10V7h2V9ZM9,9H7V7H9V9ZM6,9H4V7H6V9Z" fill="none"/>
                </clipPath>
            </defs>
            <g id="Group_4" data-name="Group 4" transform="translate(0 0)">
                <path id="Mask-2" data-name="Mask" d="M14,16H2a2,2,0,0,1-2-2V2A2,2,0,0,1,2,0H4V1A1,1,0,0,0,6,1V0h4V1a1,1,0,0,0,2,0V0h2a2,2,0,0,1,2,2V14A2,2,0,0,1,14,16ZM3,5A1,1,0,0,0,2,6v7a1,1,0,0,0,1,1H13a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1Zm9,7H10V10h2v2ZM9,12H7V10H9v2ZM6,12H4V10H6v2Zm6-3H10V7h2V9ZM9,9H7V7H9V9ZM6,9H4V7H6V9Z" fill="none"/>
                <g id="Mask_Group_58" data-name="Mask Group 58" clip-path="url(#clip-path)">
                    <g id="_Color" data-name="↳ 🎨Color">
                        <rect id="Base" width="16" height="16" fill="#a8aab7"/>
                    </g>
                </g>
            </g>
        </svg>
    </button>

    <div class="selected-date">
        <input type="text" class="selected-date-input">
    </div>

    <div class="dates" id="0">
        <div class="month">
            <div class="arrows prev-mth">&lt;</div>
            <div class="mth"></div>
            <div class="arrows next-mth">&gt;</div>
        </div>

        <div class="days"></div>
    </div>`;

    const datepickerContainer = typeof el == 'string' ? document.querySelector(el) : el;

    datepickerContainer.classList.contains('date-picker') || datepickerContainer.classList.add('date-picker')

    datepickerContainer.innerHTML = template;

    const datesElement = config.datesEl || datepickerContainer.querySelector(".dates");
    const mthElement = config.mthEl || datepickerContainer.querySelector(".dates .month .mth");
    const nextMthElement = config.nextMthEl || datepickerContainer.querySelector(".dates .month .next-mth");
    const prevMthElement = config.prevMthEl || datepickerContainer.querySelector(".dates .month .prev-mth");
    const daysElement = config.daysEl || datepickerContainer.querySelector(".dates .days");
    const calendarButton = config.calendarButtonEl || datepickerContainer.querySelector(".calendar-button");
    const selectedDateField = datepickerContainer.querySelector('.selected-date-input');

    selectedDateField.setAttribute('name', config.name);

    const months = ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'];

    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();

    // entered date, use for production
    let selectedDate = date;

    let selectedDay = day;
    let selectedMonth = month;
    let selectedYear = year;

    mthElement.textContent = months[month] + ' ' + year;

    selectedDateField.value = `${formatDate(date)}`;

    populateDates();

    calendarButton.addEventListener('click', toggleDatePicker);
    nextMthElement.addEventListener('click', goToNextMonth);
    prevMthElement.addEventListener('click', goToPrevMonth);

    function toggleDatePicker(e) {
        if (!checkEventPathForClass(e.path, 'dates')) {
            datesElement.classList.toggle('active');
        }
    }

    function goToNextMonth() {
        month++;
        if (month > 11) {
            month = 0;
            year++;
        }
        mthElement.textContent = months[month] + ' ' + year;
        populateDates();
    }

    function goToPrevMonth() {
        month--;
        if (month < 0) {
            month = 11;
            year--;
        }
        mthElement.textContent = months[month] + ' ' + year;
        populateDates();
    }

    function populateDates() {
        daysElement.innerHTML = '';
        let amountDays = new Date(year, month + 1, 0).getDate();

        for (let i = 0; i < amountDays; i++) {
            const dayElement = document.createElement('div');
            dayElement.classList.add('day');
            dayElement.textContent = `${i + 1}`;

            if (selectedDay === (i + 1) && selectedYear === year && selectedMonth === month) {
                dayElement.classList.add('selected');
            }

            dayElement.addEventListener('click', function () {
                selectedDate = new Date(year + '-' + (month + 1) + '-' + (i + 1));
                selectedDay = (i + 1);
                selectedMonth = month;
                selectedYear = year;

                selectedDateField.value = formatDate(selectedDate);
                populateDates();
            });

            daysElement.appendChild(dayElement);
        }
    }

    function checkEventPathForClass(path, selector) {
        for (let i = 0; i < path.length; i++) {
            if (path[i].classList && path[i].classList.contains(selector)) {
                return true;
            }
        }

        return false;
    }

    function formatDate(d) {
        let day = d.getDate();
        if (day < 10) {
            day = '0' + day;
        }

        let month = d.getMonth();
        if (month + 1 < 10) {
            month = '0' + (month + 1);
        } else {
            month += 1;
        }

        let year = d.getFullYear();

        return day + '/' + month + '/' + year;
    }

    return this;
}