function DatepickerY(el, config) {
    config = {
        datesEl: null,
        mthEl: null,
        nextMthEl: null,
        prevMthEl: null,
        daysEl: null,
        calendarButtonEl: null,
        selectedDateInputEl: null,
        ...config
    };

    if (!config.name) throw "Datepicker 'name' parameter is mandatory";

    const template = html`
    <button type="button" class="calendar-button">
        <svg id="Icon_Left_" data-name="Icon (Left)" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="16" viewBox="0 0 16 16" class="calendar-icon">
            <defs>
                <clipPath id="clip-path">
                    <path id="Mask" d="M14,16H2a2,2,0,0,1-2-2V2A2,2,0,0,1,2,0H4V1A1,1,0,0,0,6,1V0h4V1a1,1,0,0,0,2,0V0h2a2,2,0,0,1,2,2V14A2,2,0,0,1,14,16ZM3,5A1,1,0,0,0,2,6v7a1,1,0,0,0,1,1H13a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1Zm9,7H10V10h2v2ZM9,12H7V10H9v2ZM6,12H4V10H6v2Zm6-3H10V7h2V9ZM9,9H7V7H9V9ZM6,9H4V7H6V9Z" fill="none"/>
                </clipPath>
            </defs>
            <g id="Group_4" data-name="Group 4" transform="translate(0 0)">
                <path id="Mask-2" data-name="Mask" d="M14,16H2a2,2,0,0,1-2-2V2A2,2,0,0,1,2,0H4V1A1,1,0,0,0,6,1V0h4V1a1,1,0,0,0,2,0V0h2a2,2,0,0,1,2,2V14A2,2,0,0,1,14,16ZM3,5A1,1,0,0,0,2,6v7a1,1,0,0,0,1,1H13a1,1,0,0,0,1-1V6a1,1,0,0,0-1-1Zm9,7H10V10h2v2ZM9,12H7V10H9v2ZM6,12H4V10H6v2Zm6-3H10V7h2V9ZM9,9H7V7H9V9ZM6,9H4V7H6V9Z" fill="none"/>
                <g id="Mask_Group_58" data-name="Mask Group 58" clip-path="url(#clip-path)">
                    <g id="_Color" data-name="↳ 🎨Color">
                        <rect id="Base" width="16" height="16" fill="#a8aab7"/>
                    </g>
                </g>
            </g>
        </svg>
    </button>

    <div class="selected-date">
        <input type="text" class="selected-date-input">
    </div>

    <div class="dates" id="0">
        <div class="month">
            <div class="arrows prev-mth">&lt;</div>
            <div class="mth"></div>
            <div class="arrows next-mth">&gt;</div>
        </div>

    </div>`;

    const datepickerContainer = typeof el == 'string' ? document.querySelector(el) : el;
    datepickerContainer.classList.contains('date-picker') || datepickerContainer.classList.add('date-picker')

    datepickerContainer.innerHTML = template;
    const datesElement = config.datesEl || datepickerContainer.querySelector(".date-picker .dates");
    const mthElement = config.mthEl || datepickerContainer.querySelector(".date-picker .dates .month .mth");
    const nextMthElement = config.nextMthEl || datepickerContainer.querySelector(".date-picker .dates .month .next-mth");
    const prevMthElement = config.prevMthEl || datepickerContainer.querySelector(".date-picker .dates .month .prev-mth");
    const calendarButton = config.calendarButtonEl || datepickerContainer.querySelector(".date-picker .calendar-button");
    const selectedDateField = datepickerContainer.querySelector('.selected-date-input');

    selectedDateField.setAttribute('name', config.name);

    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();

// entered date, use for production, ignore day and month
    let selectedDate = date;

    mthElement.textContent = `${year}`;

    selectedDateField.value = `${formatDate(date)}`;

    calendarButton.addEventListener('click', toggleDatePicker);
    nextMthElement.addEventListener('click', goToNextMonth);
    prevMthElement.addEventListener('click', goToPrevMonth);
    mthElement.addEventListener('click', () => {
        selectedDate = new Date(year, month, day);
        selectedDateField.value = formatDate(selectedDate);
    });

    function toggleDatePicker(e) {
        if (!checkEventPathForClass(e.path, 'dates')) {
            datesElement.classList.toggle('active');
        }
    }

    function goToNextMonth() {
        year++;

        mthElement.textContent = `${year}`;
    }

    function goToPrevMonth() {
        year--;

        mthElement.textContent = `${year}`;
    }

    function checkEventPathForClass(path, selector) {
        for (let i = 0; i < path.length; i++) {
            if (path[i].classList && path[i].classList.contains(selector)) {
                return true;
            }
        }

        return false;
    }

    function formatDate(d) {
        return d.getFullYear();
    }

    return this;
}