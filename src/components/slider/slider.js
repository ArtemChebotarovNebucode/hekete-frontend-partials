function Slider(el, config) {
    config = {
        inputLeftEl: null,
        inputRightEl: null,
        tagSubmitEl: null,
        headerValueEl: null,
        thumbLeftEl: null,
        thumbRightEl: null,
        rangeEl: null,
        valueSeparator: ';',
        ...config
    };

    if (!config.name) throw "Slider 'name' parameter is mandatory";

    const template = html`
        <div class="slider-label">
            <label id="label-left">0 zł</label>
        </div>

        <div class="middle-container">
            <div class="slider-header">
                <p class="title">Cena</p>
                <p class="header-value" id="header-value">10000 zł - 20000 zł</p>
            </div>

            <div class="slider-area">
                <div class="multi-range-slider">
                    <input type="range" id="input-left" min="0" max="30000" value="10000" step="500">
                    <input type="range" id="input-right" min="0" max="30000" value="20000" step="500">

                    <div class="slider-body">
                        <div class="track"></div>
                        <div class="range"></div>
                        <div class="thumb left"></div>
                        <div class="thumb right"></div>
                    </div>
                </div>
            </div>

        </div>

        <div class="slider-label">
            <label id="label-right">30000 zł</label>
        </div>

        <input type="hidden" class="hidden-slider-value">`;

    const sliderContainer = typeof el == 'string' ? document.querySelector(el) : el;

    sliderContainer.classList.contains('slider') || sliderContainer.classList.add('slider');

    sliderContainer.innerHTML = template;

    const inputLeft = config.inputLeftEl || sliderContainer.querySelector("#input-left");
    const inputRight = config.inputRightEl || sliderContainer.querySelector("#input-right");
    const headerValue = config.headerValueEl || sliderContainer.querySelector("#header-value");
    const thumbLeft = config.thumbLeftEl || sliderContainer.querySelector(".thumb.left");
    const thumbRight = config.thumbRightEl || sliderContainer.querySelector(".thumb.right");
    const range = config.rangeEl || sliderContainer.querySelector(".range");
    const sliderValueField = sliderContainer.querySelector(".hidden-slider-value");

    sliderValueField.setAttribute('name', config.name);

    const setLeftValue = () => {
        let _this = inputLeft,
            min = parseInt(_this.min),
            max = parseInt(_this.max);

        _this.value = Math.min(parseInt(_this.value), parseInt(inputRight.value) - 1);

        let percent = ((_this.value - min) / (max - min)) * 100;

        thumbLeft.style.left = percent + "%";
        range.style.left = percent + "%";
    }
    setLeftValue();

    const setRightValue = () => {
        let _this = inputRight,
            min = parseInt(_this.min),
            max = parseInt(_this.max);

        _this.value = Math.max(parseInt(_this.value), parseInt(inputLeft.value) + 1);

        let percent = ((_this.value - min) / (max - min)) * 100;

        thumbRight.style.right = (100 - percent) + "%";
        range.style.right = (100 - percent) + "%";
    }
    setRightValue();

    const handleOnValueChange = () => {
        headerValue.innerHTML = `${inputLeft.value} zł - ${inputRight.value} zł`;
        sliderValueField.value = `${inputLeft.value}${config.valueSeparator}${inputRight.value}`
        console.log(sliderValueField.value);
    }

    inputLeft.addEventListener("change", handleOnValueChange);
    inputRight.addEventListener("change", handleOnValueChange);
    inputLeft.addEventListener("input", setLeftValue);
    inputRight.addEventListener("input", setRightValue);

    inputLeft.addEventListener("mouseover", function () {
        thumbLeft.classList.add("hover");
    });
    inputLeft.addEventListener("mouseout", function () {
        thumbLeft.classList.remove("hover");
    });
    inputLeft.addEventListener("mousedown", function () {
        thumbLeft.classList.add("active");
    });
    inputLeft.addEventListener("mouseup", function () {
        thumbLeft.classList.remove("active");
    });

    inputRight.addEventListener("mouseover", function () {
        thumbRight.classList.add("hover");
    });
    inputRight.addEventListener("mouseout", function () {
        thumbRight.classList.remove("hover");
    });
    inputRight.addEventListener("mousedown", function () {
        thumbRight.classList.add("active");
    });
    inputRight.addEventListener("mouseup", function () {
        thumbRight.classList.remove("active");
    });

    return this;
}

