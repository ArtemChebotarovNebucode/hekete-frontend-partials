function TagField(el, config) {

    config = {
        labelField: 'label',
        valueField: 'value',
        tagContainerEl: null,
        tagInputEl: null,
        tagSubmitEl: null,
        valueSeparator: ';',
        ...config
    };

    if (!config.name) throw "TagField 'name' parameter is mandatory";

    const template = html`
        <!-- WARNING! Set max size of tag-input value to appropriate look of tag-container -->
        <input type="text" class="tag-input gray-border">
        <button type="button" class="tag-submit gray-border">
            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8">
                <path  data-name="Plus Icon" d="M3,0V3H0V5H3V8H5V5H8V3H5V0Z" fill="#dfe3e9"/>
            </svg>
        </button>
        <input type="hidden" class="hidden-tags-value">
        <div class="tag-container gray-border"></div>`;

    const tagForm = typeof el == 'string' ? document.querySelector(el) : el;
    tagForm.classList.contains('tag-field') || tagForm.classList.add('tag-field');
    tagForm.innerHTML = template;
    const tagContainer = config.tagContainerEl || tagForm.querySelector(".tag-container");
    const tagInput = config.tagInputEl || tagForm.querySelector(".tag-input");
    const tagSubmit = config.tagSubmitEl || tagForm.querySelector(".tag-submit");
    const tagValueField = tagForm.querySelector(".hidden-tags-value");

    tagValueField.setAttribute('name', config.name);

    // array of all entered tags, use for production
    let tags = [];

    function pushTag() {
        if (tagInput.value !== '') {
            const tag = document.createElement("div");
            const tagTitle = document.createElement("p");
            const crossImage = document.createElement("img");

            const dataFiltered = config.data.filter((tag) => {
                return tag.value === tagInput.value
            });

            let label = "";

            if (dataFiltered.length > 0) {
                label = dataFiltered[0].label;
            }
            else if (config.allowCustomTags){
                label = tagInput.value;
            }

            if(label !== "") {
                crossImage.src = "../../../public/cross-gray.png"

                tag.classList.add("tag");
                crossImage.setAttribute("id", `${tags.length + 1}`);
                crossImage.classList.add("tag-delete")

                tagTitle.textContent = label;

                tags.push({
                    value: label,
                    id: tags.length + 1
                });

                tagValueField.value = tags.map(tag => tag.value).join(config.valueSeparator);

                crossImage.addEventListener("click", (e) => {
                    tagContainer.removeChild(tag);
                    tags = tags.filter((tag) => tag.id !== parseInt(e.target.getAttribute("id")));
                });

                tag.appendChild(tagTitle);
                tag.appendChild(crossImage);

                tagContainer.appendChild(tag);
                tagInput.value = "";
            }
        }
    }

    function autocomplete(inp, arr) {
        let currentFocus;
        inp.addEventListener("input", () => {
            let a, b, i, val = inp.value;

            closeAllLists();

            if (!val) { return false;}

            currentFocus = -1;

            a = document.createElement("div");
            a.setAttribute("id", "autocomplete-list");
            a.setAttribute("class", "autocomplete-items");

            inp.parentNode.appendChild(a);

            for (i = 0; i < arr.length; i++) {
                if (arr[i].value.substr(0, val.length).toUpperCase() === val.toUpperCase()) {
                    b = document.createElement("div");
                    b.innerHTML = "<strong>" + arr[i].value.substr(0, val.length) + "</strong>";
                    b.innerHTML += arr[i].value.substr(val.length);
                    b.innerHTML += "<input type='hidden' value='" + arr[i].value + "'>";

                    b.addEventListener("click", (e) => {
                        inp.value = e.target.textContent;
                        pushTag(e.target.textContent);
                        closeAllLists();
                    });

                    a.appendChild(b);
                }
            }
        });

        inp.addEventListener("keydown", (e) => {
            let x = document.getElementById("autocomplete-list");
            if (x) x = x.getElementsByTagName("div");

            if (e.keyCode === 40) {
                currentFocus++;
                addActive(x);
            } else if (e.keyCode === 38) {
                currentFocus--;
                addActive(x);
            } else if (e.keyCode === 13) {
                if (currentFocus > -1) {
                    if (x) x[currentFocus].click();
                }
            }
        });
        function addActive(x) {
            if (!x) return false;

            removeActive(x);

            if (currentFocus >= x.length) currentFocus = 0;
            if (currentFocus < 0) currentFocus = (x.length - 1);

            x[currentFocus].classList.add("autocomplete-active");
        }

        function removeActive(x) {
            for (let i = 0; i < x.length; i++) {
                x[i].classList.remove("autocomplete-active");
            }
        }

        function closeAllLists(element) {
            const x = document.getElementsByClassName("autocomplete-items");

            for (let i = 0; i < x.length; i++) {
                if (element !== x[i] && element !== inp) {
                    x[i].parentNode.removeChild(x[i]);
                }
            }
        }

        document.addEventListener("click", function (e) {
            closeAllLists(e.target);
        });

        tagSubmit.addEventListener("click", (e) => {
            e.preventDefault();
            pushTag();
        });

        tagInput.addEventListener("keydown", (e) => {
            if (e.keyCode === 13) {
                pushTag();
                closeAllLists();
            }
        });

    }

    autocomplete(tagInput, config.data);

    return this;
}
