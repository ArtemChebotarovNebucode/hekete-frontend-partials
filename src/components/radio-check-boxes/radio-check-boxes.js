function CircledCheckBox(el) {
    const template = html`
        <div class="checkbox-group">
            <input type="checkbox" class="checkbox" id="id"/>
            <label for="id" class="custom-circled-checkbox">Value</label>
        </div>`;

    const checkBoxGroup = typeof el == 'string' ? document.querySelector(el) : el;

    checkBoxGroup.classList.contains('checkbox-group') || checkBoxGroup.classList.add('checkbox-group');

    checkBoxGroup.innerHTML = template;

    return this;
}

function SquaredCheckBox(el) {
    const template = html`
        <div class="checkbox-group">
            <input type="checkbox" id="id" />
            <label for="id" class="custom-squared-checkbox">Value</label>
        </div>`;

    const checkBoxGroup = typeof el == 'string' ? document.querySelector(el) : el;

    checkBoxGroup.classList.contains('checkbox-group') || checkBoxGroup.classList.add('checkbox-group');

    checkBoxGroup.innerHTML = template;

    return this;
}


function RadioButtonsGroup(el) {

    const template = html`
        <div class="radio-group">
            <label class="custom-radio-button">
                <input type="radio" value="value-first" name="name" checked>
                Value
                <span></span>
            </label>
            <label class="custom-radio-button">
                <input type="radio" value="value-second" name="name">
                Value
                <span></span>
            </label>
        </div>`;

    const radioButtonsGroup = typeof el == 'string' ? document.querySelector(el) : el;

    radioButtonsGroup.classList.contains('radio-group') || radioButtonsGroup.classList.add('radio-group');

    radioButtonsGroup.innerHTML = template;

    return this;
}










