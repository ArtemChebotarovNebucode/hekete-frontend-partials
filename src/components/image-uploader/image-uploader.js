function ImageUploader(el, config) {

    config = {
        uploadAreaEl: null,
        uploadAreaContentEl: null,
        fileInputEl: null,
        fileInputFooterEl: null,
        uploadAreaAfterContentEl: null,
        uploadAreaAfterImages: null
    };

    const template = html`
        <div class="upload-area-before">
                <div class="upload-area-content">
                    <p>Przeciągnij i upuść zdjęcia tutaj</p>
                    <p>lub</p>
                    <div class="upload-input-wrap">
                        <label for="file-input" class="instruction">Wybierz zdjęcia z dysku</label>
                    </div>
                </div>
                <input type="file" class="file-input" accept="image/x-png,image/jpeg" multiple>
            </div>

            <div class="upload-area-after-content" style="display: none">
                <div class="upload-area-after-images">

                </div>
                <hr />
                <div class="upload-footer">
                    <div class="upload-input-footer-wrap">
                        <svg xmlns="http://www.w3.org/2000/svg" width="29.08" height="32.093" viewBox="0 0 29.08 32.093">
                            <g id="upload" transform="translate(-3.007 -1)">
                                <path id="Path_449" data-name="Path 449" d="M6.454,11.815a5.935,5.935,0,0,0-5.913,5.91v7.382a5.935,5.935,0,0,0,5.913,5.91H23.712a5.932,5.932,0,0,0,5.91-5.91V17.725a5.932,5.932,0,0,0-5.91-5.91,1.609,1.609,0,1,0,0,3.218A2.651,2.651,0,0,1,26.4,17.725v7.382A2.651,2.651,0,0,1,23.712,27.8H6.454a2.651,2.651,0,0,1-2.692-2.692V17.725a2.651,2.651,0,0,1,2.692-2.692,1.609,1.609,0,0,0,0-3.218Z" transform="translate(2.465 2.077)" fill="#FFF" opacity="0.998"/>
                                <path id="Path_450" data-name="Path 450" d="M14.4.2a1.582,1.582,0,0,0-.238.02l-.093.017A1.583,1.583,0,0,0,13.8.32q-.044.018-.087.039a1.721,1.721,0,0,0-.315.2q-.036.03-.07.062l-.023.022L6.679,7.278a1.53,1.53,0,0,0-.229.3q-.024.041-.045.083a1.582,1.582,0,0,0-.076.172q-.017.044-.031.09t-.025.09a1.581,1.581,0,0,0,.094,1.013q.019.043.04.085a1.583,1.583,0,0,0,.1.162q.027.039.056.076a1.583,1.583,0,0,0,.124.142q.033.033.068.065a1.583,1.583,0,0,0,.306.216q.041.023.084.043a1.57,1.57,0,0,0,.826.141l.093-.011.093-.019L8.244,9.9l.09-.029.088-.032q.043-.018.085-.039a1.582,1.582,0,0,0,.408-.288l3.958-3.96.071,9.586q0,.047,0,.094a1.585,1.585,0,0,0,.048.278,1.565,1.565,0,0,0,.059.178,1.54,1.54,0,0,0,.3.476q.031.035.065.068a1.573,1.573,0,0,0,.218.179q.039.027.08.051t.082.045q.042.022.085.042a1.585,1.585,0,0,0,.92.111l.093-.019a1.578,1.578,0,0,0,.266-.09,1.557,1.557,0,0,0,.168-.087q.04-.024.079-.049t.077-.056q.037-.028.073-.059t.07-.063a1.582,1.582,0,0,0,.184-.213q.028-.039.053-.079t.046-.08a1.471,1.471,0,0,0,.162-.441q.01-.046.017-.093a1.589,1.589,0,0,0,.017-.187c0-.01,0-.021,0-.031l-.073-9.586,4.118,4q.035.033.071.063t.074.057q.038.028.077.054c.026.017.052.034.079.049a1.587,1.587,0,0,0,1,.193l.093-.015.091-.022c.031-.008.061-.017.091-.026s.059-.02.088-.031.058-.024.087-.037.056-.027.084-.042.055-.031.082-.048.053-.033.079-.051a1.7,1.7,0,0,0,.28-.25q.031-.035.06-.073a1.582,1.582,0,0,0,.193-.322q.02-.043.037-.087c.011-.03.02-.059.029-.09s.018-.06.026-.09a1.579,1.579,0,0,0,.045-.467c0-.031,0-.062-.008-.093a1.6,1.6,0,0,0-.031-.186,1.581,1.581,0,0,0-.13-.354q-.021-.042-.045-.082t-.049-.08q-.026-.039-.054-.076a1.569,1.569,0,0,0-.122-.144l-.022-.022L15.528.63a1.58,1.58,0,0,0-.221-.175c-.026-.017-.053-.033-.08-.048s-.055-.031-.084-.045-.056-.027-.085-.039A1.577,1.577,0,0,0,14.511.2l-.056,0Z" transform="translate(3.021 0.802)" fill="#FFF"/>
                            </g>
                        </svg>

                        <label for="file-input-footer" class="instruction-footer">Dodaj zdjęcia</label>
                        <input type="file" class="file-input-footer" accept="image/x-png,image/jpeg" multiple>
                    </div>
            </div>
        </div>`;

    const imageUploader = typeof el == 'string' ? document.querySelector(el) : el;

    imageUploader.classList.contains('image-uploader') || imageUploader.classList.add('image-uploader');

    imageUploader.innerHTML = template;

    const uploadArea = config.uploadAreaEl || imageUploader.querySelector(".upload-area-before");
    const uploadAreaContent = config.uploadAreaAfterContentEl ||
        imageUploader.querySelector(".upload-area-content");
    const fileInput = config.fileInputEl || imageUploader.querySelector(".file-input");
    const fileInputFooter = config.fileInputFooterEl || imageUploader.querySelector(".file-input-footer");
    const uploadAreaAfterContent = config.uploadAreaAfterContentEl ||
        imageUploader.querySelector(".upload-area-after-content");
    const uploadAreaAfterImages = config.uploadAreaAfterImages ||
        imageUploader.querySelector(".upload-area-after-images");

    // WARNING! There is no possibility to place all uploaded files to one result input
    // in this case you need to get files from array but not from hidden input

    // array of all uploaded files, use for production
    let uploadedFiles = [];

    const clearNode = (parent) => {
        while (parent.firstChild) {
            parent.removeChild(parent.firstChild);
        }
    }

    const srcToFile = async (src, fileName, mimeType) => {
        return await (fetch(src)
                .then(function (res) {
                    return res.arrayBuffer();
                })
                .then(function (buf) {
                    return new File([buf], fileName, {type: mimeType});
                })
        );
    }

    const renderUploadedImages = () => {
        for (let i = 0; i < uploadedFiles.length; i++) {
            const fileName = uploadedFiles[i].name;

            const reader = new FileReader();
            reader.onload = function () {
                const img = new Image();
                const imgOriginal = new Image();
                const rotateImg = document.createElement("img");

                const imageContainer = document.createElement("div");
                const crossImg = document.createElement("img");
                const imageOverlay = document.createElement("div");

                img.classList.add("image-uploaded");
                img.crossorigin = "anonymous";
                img.onload = () => {
                    imageOverlay.appendChild(rotateImg);
                    imageOverlay.appendChild(crossImg);
                    imageContainer.appendChild(img);
                    imageContainer.appendChild(imageOverlay);
                    uploadAreaAfterImages.appendChild(imageContainer);
                }
                img.src = reader.result.toString();
                imgOriginal.src = reader.result.toString();

                crossImg.src = "../../../public/cross-white.png";
                crossImg.classList.add("cross-image");
                rotateImg.src = "../../../public/rotate.png";
                rotateImg.classList.add("rotate-image");

                imageOverlay.classList.add("image-overlay");
                imageContainer.classList.add("image-container");

                crossImg.addEventListener("click", () => {
                    uploadedFiles = uploadedFiles.filter((object) => {
                        return object.name !== fileName
                    });
                    uploadAreaAfterImages.removeChild(imageContainer);

                    if (uploadedFiles.length === 0) {
                        uploadArea.classList.remove("upload-area-after");
                        uploadArea.classList.add("upload-area-before");
                        uploadArea.appendChild(uploadAreaContent);
                        uploadArea.appendChild(fileInput);
                        uploadAreaAfterContent.style.display = "none";
                    }
                });


                rotateImg.addEventListener("click", () => {
                    const c = document.createElement("canvas");
                    c.width = imgOriginal.height;
                    c.height = imgOriginal.width;
                    const ctx = c.getContext("2d");

                    ctx.translate(c.width / 2, c.height / 2);
                    ctx.rotate(Math.PI / 2);
                    ctx.drawImage(imgOriginal, -imgOriginal.width / 2, -imgOriginal.height / 2);
                    ctx.rotate(-Math.PI / 2);
                    ctx.translate(-c.width / 2, -c.height / 2);

                    srcToFile(c.toDataURL(), fileName.split(".")[0], fileName.split(".")[1])
                        .then(newFile => {
                            uploadedFiles = uploadedFiles.map((object) => {
                                return object.name === fileName ? {
                                    file: newFile,
                                    name: fileName
                                } : object
                            });

                            clearNode(uploadAreaAfterImages);
                            renderUploadedImages();
                        });
                });
            }

            reader.readAsDataURL(uploadedFiles[i].file);
        }
    }

    fileInput.addEventListener("change", (e) => {
        uploadedFiles = [];

        if (e.target.files.length > 0) {
            uploadArea.classList.remove("upload-area-before");
            uploadArea.classList.add("upload-area-after");

            for (let i = 0; i < fileInput.files.length; i++) {
                uploadedFiles.push({
                    file: fileInput.files[i],
                    name: fileInput.files[i].name
                });
            }

            clearNode(uploadArea);

            renderUploadedImages();

            uploadArea.appendChild(uploadAreaAfterContent);
            uploadAreaAfterContent.style.display = "block";

            fileInput.value = "";
        }
    });

    fileInputFooter.addEventListener("change", () => {

        for (let i = 0; i < fileInputFooter.files.length; i++) {
            if (!uploadedFiles.filter(e => e.name === fileInputFooter.files[i].name).length > 0) {
                uploadedFiles.push({
                    file: fileInputFooter.files[i],
                    name: fileInputFooter.files[i].name
                });
            }
        }

        clearNode(uploadAreaAfterImages);

        renderUploadedImages();

        console.log(uploadedFiles)
    });

    return this;
}


