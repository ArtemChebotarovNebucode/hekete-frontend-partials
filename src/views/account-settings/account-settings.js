const userPanelButton = document.querySelector("#user-panel-button");
const settingsButton = document.querySelector("#settings-button");
const settingsOptions = document.querySelectorAll(".settings-option");

const userPanel = document.querySelector(".user-panel");
const settingsPanel = document.querySelector(".setting-panel");

const salesCountCTX = document.getElementById('sales-count').getContext('2d');
new Chart(salesCountCTX, {
    type: 'bar',
    data: {
        labels: ['2015', '2016', '2017', '2018', '2019', '2020'],
        datasets: [{
            label: 'liczba sprzedaż',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: [
                'rgba(96, 137, 171, 0.7)',
                'rgba(96, 137, 171, 0.7)',
                'rgba(96, 137, 171, 0.7)',
                'rgba(96, 137, 171, 0.7)',
                'rgba(96, 137, 171, 0.7)',
                'rgba(96, 137, 171, 0.7)',
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
        maintainAspectRatio: false,
    }
});

const pieData = {
    datasets: [{
        data: [2, 12],
        backgroundColor: [
            'rgba(38, 109, 79, 1)',
            'rgb(66,185,135)',
        ]
    }],
    labels: [
        'Pojazdy',
        'Części samochodowe'
    ]
};

const purchaseTypesCTX = document.getElementById("purchase-types").getContext("2d");
new Chart(purchaseTypesCTX, {
    type: 'pie',
    data: pieData,
    options: {
        maintainAspectRatio: false,
    }
});

const visitsCTX = document.getElementById("visits-count").getContext("2d");
new Chart(visitsCTX, {
    type: 'line',
    data: {
        labels: ['Styczeń', 'Luty', 'Marzec', 'Kwiecień', 'Maj', 'Czerwiec', 'Lipiec', 'Sierpień', 'Wrzesień', 'Październik', 'Listopad', 'Grudzień'],
        datasets: [{
            label: 'liczba odwiedzeń twoich ogłoszeń',
            data: [68, 71, 24, 91, 120, 275, 90, 345, 124, 41, 32, 45, 98],
            backgroundColor: [
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)',
                'rgba(2, 75, 135, 0.7)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        maintainAspectRatio: false,
    }
});

const purchaseCountCTX = document.getElementById("purchase-count").getContext("2d");
new Chart(purchaseCountCTX, {
    data: {
        labels: ['2018', '2019', '2020'],
        datasets: [{
            label: 'liczba sprzedaż',
            data: [3, 4, 7],
            backgroundColor: [
                'rgba(200, 241, 224, 0.7)',
                'rgba(142, 246, 202, 0.7)',
                'rgba(66, 185, 135, 0.7)'
            ],
            borderWidth: 1
        }]
    },
    type: 'polarArea',
    options: {
        maintainAspectRatio: false,
    }
});

userPanelButton.addEventListener("click", () => {
    userPanel.style = "display: flex";
    settingsPanel.style = "display: none";

    deactivateButtons();

    userPanelButton.classList.add("active");
});

settingsButton.addEventListener("click", () => {
    userPanel.style = "display: none";
    settingsPanel.style = "display: block";

    deactivateButtons();

    settingsButton.classList.add("active");
});

function deactivateButtons() {
    for (let i = 0; i < settingsOptions.length; i++) {
        settingsOptions[i].classList.remove("active");
    }
}



