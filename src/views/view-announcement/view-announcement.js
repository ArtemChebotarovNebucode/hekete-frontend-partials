const detailsOptions = document.querySelectorAll(".details-option");

const equipmentButton = document.querySelector("#equipment-button");
const historyButton = document.querySelector("#history-button");
const descriptionButton = document.querySelector("#description-button");
const detailsButton = document.querySelector("#details-button");

const equipmentContainer = document.querySelector("#vehicle-equipment-gap");
const detailsContainer = document.querySelector("#vehicle-details-gap");
const descriptionContainer = document.querySelector("#vehicle-description-gap");
const historyContainer = document.querySelector("#vehicle-history-gap");
const slideShowContainer = document.querySelector(".slideshow-container");
const slideShowContainerExtended = document.querySelector(".slideshow-container-extended");
const fullScreenModal = document.querySelector("#full-screen-modal");
const fullScreenImage = document.querySelector("#full-screen-image");
const crossSpan = document.querySelectorAll(".close")[0];


const imagesPaths = [
    "https://i.wpimg.pl/730x0/m.autokult.pl/c8522323-peugeot-508-spo-450f194.jpg",
    "https://moto.rp.pl/wp-content/uploads/2019/02/Peugeot-508_Sport_Engineered_Concept-2.jpg",
    "https://www.wyborkierowcow.pl/wp-content/uploads/2019/02/Peugeot_508PSE_1902PJ_105.jpg",
    "https://media.peugeot.pl/image/18/3/peugeot-508-miejsce-kierowcy-desktop.556183.1.jpg?autocrop=1",
    "https://www.autocentrum.pl/ac-file/gallery-photo/5e40759ac74b357ec2182593/peugeot-508.jpg"
];

for (let i = 0; i < detailsOptions.length; i++) {
    detailsOptions[i].addEventListener("click", (e) => {
        for (let j = 0; j < detailsOptions.length; j++) {
            detailsOptions[j].classList.remove("active");
        }
        e.target.classList.add("active");
    });
}

for (let i = 0; i < imagesPaths.length; i++) {
    const mySlidesDiv = document.createElement("div");
    const mySlidesImg = document.createElement("img");

    mySlidesImg.onclick = function(){
        fullScreenModal.style.display = "flex";
        fullScreenImage.src = mySlidesImg.src;
    }

    mySlidesDiv.classList.add("mySlides");
    mySlidesDiv.classList.add("fade");
    mySlidesImg.src = imagesPaths[i];

    const carouselImg = mySlidesImg.cloneNode(true);
    carouselImg.classList.add("mySlidesExtended");

    carouselImg.addEventListener("click", () => {
       showSlidesExtended(i, i+3);
    });

    mySlidesDiv.appendChild(mySlidesImg);
    slideShowContainer.appendChild(mySlidesDiv);
    slideShowContainerExtended.appendChild(carouselImg);
}

let slideIndex = 0;
// showSlides(slideIndex);
showSlidesExtended(slideIndex, slideIndex + 3);

function plusSlides(n) {
    // showSlides(slideIndex += n);
    showSlidesExtended(slideIndex += n, slideIndex + 3);
}


function showSlides(n) {
    let i;
    const slides = document.getElementsByClassName("mySlides");
    if (n > slides.length) {n = 0}
    if (n < 0) {n = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }

    if (n === slides.length) {
        n--;
    }

    slides[n].style.display = "block";
}

function showSlidesExtended(s, f) {
    let i;
    let outOfBounds = 0;
    const slides = document.getElementsByClassName("mySlidesExtended");

    showSlides(s);
    if (f > slides.length) {
        outOfBounds = f - slides.length;
        f = slides.length
    }

    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
        slides[i].classList.remove("selected");
    }
    for (i = s; i < f; i++) {
        slides[i].style.display = "block";
    }

    if (outOfBounds !== 0) {
        for (let i = 0; i < outOfBounds; i++) {
            slides[i].style.display = "block";
        }
    }

    slides[s].classList.add("selected");
}

function checkVisible(elm) {
    const rect = elm.getBoundingClientRect();
    const viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
    return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}

const clearNode = (parent) => {
    while (parent.firstChild) {
        parent.removeChild(parent.firstChild);
    }
}

equipmentButton.addEventListener("click", () => {
    equipmentContainer.scrollIntoView();
});

detailsButton.addEventListener("click", () => {
    detailsContainer.scrollIntoView();
});

descriptionButton.addEventListener("click", () => {
    descriptionContainer.scrollIntoView();
});

historyButton.addEventListener("click", () => {
    historyContainer.scrollIntoView();
});

document.addEventListener('scroll', function () {
    for (let i = 0; i < detailsOptions.length; i++) {
        detailsOptions[i].classList.remove("active");
    }

    if (checkVisible(detailsContainer)) {
        detailsButton.classList.add("active");
    } else if (checkVisible(historyContainer)) {
        historyButton.classList.add("active");
    } else if (checkVisible(descriptionContainer)) {
        descriptionButton.classList.add("active");
    } else if (checkVisible(equipmentContainer)) {
        equipmentButton.classList.add("active");
    }

}, {
    passive: true
});

crossSpan.onclick = function() {
    fullScreenModal.style.display = "none";
}





